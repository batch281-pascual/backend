const express = require('express');
const mongoose = require('mongoose');
const cors = require("cors");

const userRoutes = require("./routes/userRoutes");
const courseRoutes = require("./routes/courseRoutes");

// Initialization of express function
const app = express();

// Connnect to our MongoDB using mongoose method
mongoose.connect("mongodb+srv://mjordanpascual:FGRcaKmOh9QH41yO@wdc028-course-booking.sbuuomu.mongodb.net/s37-41API?retryWrites=true&w=majority",{
	useNewUrlParser: true,
	useUnifiedTopology: true
});

let db = mongoose.connection;
 
 // OPTION 1 db connection notification 
// db.on('error', console.error.bind(console, "MondoDB connnection Error."));
// db.once('open', () => console.log('Now Connected to MongoDB Atlas!'))

// OPTION 2 db connection notification
mongoose.connection.once('open', () => console.log('Now Connected to MongoDB Atlas!'))

// Allows all resources to access our backend application
app.use(cors());

app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the "/users" string to be included for all user routes defined in the "userRoutes" file
app.use("/users", userRoutes );

app.use("/courses", courseRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
});

