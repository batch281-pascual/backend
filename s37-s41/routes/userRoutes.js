const express = require('express');
const router = express.Router();
const userController = require("../controllers/userController");
const auth = require("../auth");

// Route for checking if the email already exists in the database
// Invokes the checkEmailExists function from the controller file
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})

// Route for authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));	
})



/* ---------------------------------------------------------- */
// ACTIVITY

// The auth.verify acts as a middleware to ensure that the user is logged in before they cant enroll to a course

// router.get("/details", auth.verify, (req, res) => {

// 	// Uses decode method defined in the auth.js file to retrieve user information from the token passing the "token" from the request header

// 	const userData = auth.decode(req.headers.authorization);

// 	userController.getProfile(req.body).then(resultFromController => res.send(resultFromController))

// });

// 2ND OPTION IN 	ACTIVITY
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);


	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController))
})





/* ----------------------------------------------------------- */
// DISCUSSION CODE S41.

// ORIGINAL CODEBASE WITHOUT USER isAdmin AUTHENTICATION.

// ROUTE TO ENROLL USER TO A COURSE
// router.post("/enroll", (req, res) => {

// 	let data = {
// 		userId : req.body.userId,
// 		courseId : req.body.courseId
// 	}

// 	userController
// 	.enroll(data)
// 	.then(resultFromController => res.send(resultFromController))
// })

/* ----------------------------------------------------------- */




/* ----------------------------------------------------------- */

// ACTIVITY: s41

// router.post("/enroll", auth.verify, (req, res) => {

// 	// let userData = {
// 	// 	userId : auth.decode(req.headers.authorization).id,
// 	// 	courseId : req.body.courseId,
// 	// 	isAdmin : auth.decode(req.headers.authorization).isAdmin
// 	// }	

// 	let data = {
// 		userData : auth.decode(req.headers.authorization),
// 		courseId : req.body.courseId
// 	}	

// 	if(userData.isAdmin == true){
// 		userController
// 		.enroll(userData)
// 		.then(resultFromController => res.send(resultFromController))	
// 	} else {
// 		res.send(false);
// 	}

// })



router.post("/enroll", auth.verify, (req, res) => {

	let data = {
		userData : auth.decode(req.headers.authorization),
		courseId : req.body.courseId
	}

	userController.enroll(data).then(resultFromController => res.send(resultFromController))
	// userController.enroll(data))

})


// router.delete("/:id", (req, res) => {
// 	userController.deleteUser(req.params.id, req.body)
// 	.then(resultFromController => res.send(resultFromController))
// })



/* ----------------------------------------------------------- */



module.exports = router;

