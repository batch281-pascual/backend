//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
// async function getSingleToDo(){

//     return await (
       
//        fetch('https://jsonplaceholder.typicode.com/todos/1', {
//          method: 'GET'
//        })
//        .then((response) => response.json())
//        .then((json) => json)
//    );
// }

async function getSingleToDo(){

    return await (
       
       let result = fetch('https://jsonplaceholder.typicode.com/todos/1')
       let getFetch = response.json();
       .then((json) => {
         console.log(json);
       })
   );
}
getSingleToDo();


// Getting all to do list item
async function getAllToDo(){

   return await (
      fetch('https://jsonplaceholder.typicode.com/todos', {
         method: 'GET'
      })
      .then((response) => response.json())
      .then((json) => {
         let titles = json.map((todo) => todo.title);
         return titles;
      })
  );
}

getAllToDo()

// [Section] Getting a specific to do list item
async function getSpecificToDo(){
   
   return await (

       fetch('https://jsonplaceholder.typicode.com/todos?userId=3', {
         method: 'GET'
       })
       .then((response) => response.json())
       .then((json) => json)
   );
}


// [Section] Creating a to do list item using POST method
async function createToDo(){
   
   return await (

       fetch('https://jsonplaceholder.typicode.com/todos', {
         method: 'POST',
         headers: {
            'Content-Type': 'application/json'
         },
         body: JSON.stringify({
              completed: false,
              id: 201,
              title: "Created To Do List Item",
              userId: 1
         }),
       })
       .then((response) => response.json())
   );
}

// [Section] Updating a to do list item using PUT method
async function updateToDo(){
   
   return await (

       fetch('https://jsonplaceholder.typicode.com/todos/1', {
         method: 'PUT',
         headers: {
            'Content-Type': 'application/json'
         },
         body: JSON.stringify({
              dateCompleted: "Pending",
              description: "To update the my to do list with a different data structure",
              id: 1,
              status: "Pending",
              title: "Updated To Do List Item",
              userId: 1 
         }),
       })
       .then((response) => response.json())
   );
}

// [Section] Deleting a to do list item
async function deleteToDo(){
   
   return await (

       fetch('https://jsonplaceholder.typicode.com/todos/1', {
         method: 'DELETE'
      })
   );
}


//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}


