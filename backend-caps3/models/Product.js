const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({

	name : {
		type: String,
		required :[true, "Product is required."]
	},
	description: {
		type: String,
		required :[true, "Description is required."]
	},
	price : {
		type: Number,
		required :[true, "Price is required."]
	},
	imageUrl: {
 		type: String,
 		default: "https://www.och-lco.ca/wp-content/uploads/2015/07/unavailable-image.jpg"
 	},
	isActive : {
		type: Boolean,
		default: true
	},
	createdOn : {
		type: Date,
		default: new Date()
	},
    userOrders : [{
        userId : {
            type : String
        },
        orderId : {	
            type : String
        }
    }]

});

module.exports = mongoose.model("Product", productSchema);



