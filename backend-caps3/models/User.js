const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	isAdmin: {
		type: Boolean,
		default: false 
	},
	mobileNo: {
		type: String,
		required: [true, "Mobile Number is required."]
	},
    orderedProduct : [{
        products : [{
            productId : {
                type : String
            },
            productName : {
                type : String,
                required : [true, "Product Name is requred"]
            },
            productImageUrl : {
            	type : String
            },
            quantity : {
                type : Number,
                required : [true, "Quantity is requred"]
            }
        }],
        totalAmount : {
            type : Number,
            default : 0
        },
        purchasedOn : {
            type : Date,
            default : new Date()
        }
    }]
});

module.exports = mongoose.model("User", userSchema);