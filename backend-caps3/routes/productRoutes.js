const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");


/* --------------------------------------------------------------------------- */
// ACTIVITY SOLUTION: s39


// Route for creating a product
router.post("/addProduct", auth.verify, (req, res) => {
	
	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		productController.addProduct(data).then(resultFromController => res.send(resultFromController));
	} else {
		res.send(false);
	}

});


/* --------------------------------------------------------------------------- */


// Route for retrieving all the product
router.get("/all", (req, res) => {
	productController
	.getAllProducts()
	.then(resultFromController => res.send(resultFromController))
})


// Route for retrieving all the active courses
router.get("/", (req, res) => {
	productController
	.getAllActive()
	.then(resultFromController => res.send(resultFromController))
})


// Route for retrieving a specific course
// Creating a course using "/:parametername" creates a dynamic route, meaning the URL changes depending on the information provided
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	// Since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the course ID by accessing the request's "params" property which contains all the parameters provided via the URL

	productController
	.getProduct(req.params)
	.then(resultFromController => res.send(resultFromController))
})


// Route for updating a product
router.put("/:productId", auth.verify, (req, res) => {
	productController
	.updateProduct(req.params, req.body)
	.then(resultFromController => res.send(resultFromController))
})




/* ----------------------------------------------------------------- */

// ACTIVITY SOLUTION: OPTION1

// router.put("/:courseId/archive", auth.verify, (req, res) => {

// 	const data = {
// 		isAdmin: auth.decode(req.headers.authorization).isAdmin
// 	}

// 	if(data.isAdmin == true){
// 		courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
// 	} else {
// 		res.send(false);
// 	}
	
// });


// ACTIVITY: s40

router.patch("/:productId", auth.verify, (req, res) => {

	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true) {
		productController
		.archiveProduct(req.params, req.body)
		.then(resultFromController => res.send(resultFromController))	
	} else {
		res.send("false");
	}
	
})



router.patch("/:productId", auth.verify, (req, res) => {
	const data = {
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	if(data.isAdmin == true) {
		productController
		.unArchiveProduct(req.params, req.body)
		.then(resultFromController => res.send(resultFromController))	
	} else {
		res.send("false");
	}

})


/* ----------------------------------------------------------------- */



router.delete("/:id", (req, res) => {
	productController.deleteProduct(req.params.id, req.body)
	.then(resultFromController => res.send(resultFromController))
})





// Allows us to export the "router" object that will be accessed in our index.js file

module.exports = router;
