let http = require('http');

// Mock database
let directory = [
	{ 
		"name": "Brandon",
		"email": "brandon@mail.com",
	},
	{ 
		"name": "Jobert",
		"email": "jobert@mail.com",
	}
];

let port = 4000;

let app = http.createServer( (req, res) => {
	// Route for returning all items upon receiving a GET request
	// Sets status code 200 and response output to JSON data
	if(req.url == "/users" && req.method == "GET"){
		res.writeHead(200, {'Content-Type': 'application/json'});
		res.write(JSON.stringify(directory));
		res.end();
	}

	// Route for creating a new data upon receiving a POST request
	if(req.url == '/users' && req.method == 'POST'){
		// "requestBody" acts as a placeholder for the resource/data to be created later on
		let requestBody = '';

		// Stream is a sequence of data
		req.on('data', function(data){
			// Assigns the data retrieved from the data stream to requestBody
			requestBody += data;
		});

		req.on('end', function(){
			console.log(typeof requestBody);

			// Converts the string requestBody to JSON
			requestBody = JSON.parse(requestBody);

			// Creats a new object representing the new mock database
			let newUser = {
				"name": requestBody.name,
				"email": requestBody.email
			};

			// Add the new user to the mock database
			directory.push(newUser);
			console.log(directory);

			res.writeHead(200, {'Content-Type': 'application/json'});
			res.write(JSON.stringify(newUser));
			res.end();
		});
	}
});

app.listen(port, () => console.log(`Server running at localhost: ${port}.`) );