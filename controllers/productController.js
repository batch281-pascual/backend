const Product = require("../models/Product")
const User = require("../models/User");
const auth = require("../auth");


// Create new product
module.exports.addProduct = (data) =>{

	// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newProduct = new Product({
		name : data.product.name,
		description : data.product.description,
		price : data.product.price,
		imageUrl : data.product.imageUrl
	});

	return newProduct.save().then((product, error) => {
		if(error){
			return false;

		}else {
			return true;
		};
	})
};



// Retrieve all product
module.exports.getAllProducts = () => {
	return Product.find({}).then(result => {
		return result;
	})
}



// Retrieve all active product
module.exports.getAllActive = () => {
	return Product
	.find({ isActive : true })
	.then(result => {
		return result;
	});
};



// Retrieving a specific product
module.exports.getProduct = (reqParams) => {
	return Product
	.findById(reqParams.productId)
	.then(result => {
		return result
	})
}



// Update a product
module.exports.updateProduct = (reqParams, reqBody) => {

	// Specify the fields/properties of the document to be updated

	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price,
		imageUrl : reqBody.imageUrl
	}

	return Product
	.findByIdAndUpdate(reqParams.productId, updatedProduct)
	.then((product, error) => {

		// Product not updated
		if(error) {
			return false;

		// Product updated successfully 
		} else {
			return true;
		}
	})

	return Product
	.findById(reqParams.productId)
	.then(result => {
		return result
	})
}



/* ------------------------------------------------------ */
// ACTIVITY: s40


// ACTIVITY SOLUTION: OPTION1

// module.exports.archiveCourse = (reqParams) => {

// 	let updateActiveField = {
// 		isActive : false
// 	};

// 	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

// 		// Course not archived
// 		if (error) {

// 			return false;

// 		// Course archived successfully
// 		} else {

// 			return true;

// 		}

// 	});
// };



module.exports.archiveProduct = (reqParams, reqBody) => {
	let updatedProduct = {
		isActive : reqBody.isActive
		// isActive : false
	}

	return Product
	.findByIdAndUpdate(reqParams.productId, updatedProduct)
	.then((product, error) => {

		if(error) {
			return false;

		} else {
			return true;
		}
	})
}


module.exports.unArchiveProduct = (reqParams, reqBody) => {
	let unArchiveProduct = {
		// isActive : reqBody.isActive
		isActive : true
	}

	return Product
	.findByIdAndUpdate(reqParams.productId, unArchiveProduct)
	.then((product, error) => {
		if(error) {
			return false;
		} else {
			return true;
		}
	})
}

/* ------------------------------------------------------ */


// Deleting Product Item
module.exports.deleteProduct = (productId) => {
	return Product.findByIdAndRemove(productId)
	.then((removeProduct, error) => {
		if(error){
			return false;
		} else {
			return true;
		}
	})
}


