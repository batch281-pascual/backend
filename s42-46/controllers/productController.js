const User = require('../models/User');
const Product = require('../models/Product')
const bcrypt = require('bcrypt');
const auth = require('../auth');


module.exports.addProduct = (reqBody) => {

	let newProduct = new Product ({
		name : reqBody.name,
		description : reqBody.description,
		productName : reqBody.productName,
		price : reqBody.price
	})

	return newProduct.save().then((product, error) => {
		if(error)
{			return false
		} else {
			return (`Product Name: ${product.name} \n\ Price: ${product.price} \n\ \n\ Successfully Added New Product.`);
		};
	})
};



module.exports.getAllProducts = () => {
	return Product.find({}).then((result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	}) 
}


module.exports.allActiveProducts = () => {
	return Product
	.find({ isActive : true })
	.then(result => {
		return result
	})
} 


module.exports.inactiveProducts = () => {
	return Product.find({ isActive : false })
	.then(result => {
		return result;
	})
}



module.exports.singleProduct = (reqParams) => {
	return Product
	.findById(reqParams.id)
	.then(result => {
		return result
	})
}



module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}

	return Product
	.findByIdAndUpdate(reqParams.id, updatedProduct)
	.then((product, error) => {
		if(error){
			return false
		} else {
			return ('Successfully updated product.');
		}
	})
}


module.exports.archiveProduct = (reqParams, reqBody) => {

	let archivedProduct = {
		isActive : false
	}

	return Product
	.findByIdAndUpdate(reqParams.id, archivedProduct)
	.then((result, error) => {
		if(error) {
			return false
		} else {
			// return result
			return (`Product Name: ${result.name}
				\n\ (Set to Inactive.)`);
		}
	})


}


module.exports.activateProduct = (reqParams, reqBody) => {

	let activatedProduct = {
		isActive : true
	}

	return Product
	.findByIdAndUpdate(reqParams.id, activatedProduct)
	.then((result, error) => {
		if(error){
			return false
		} else {
			return result
		}
	})
}


module.exports.deleteCourse = (courseId) => {
	return Product.findByIdAndRemove(courseId)
	.then((result, error) => {
		if(error){
			return ('Deleting product unsuccesfull!');
		} else {
			return ('Successfully deleted!');
		}
	})
}
