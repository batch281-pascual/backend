const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');


// module.exports.registerUser = (reqBody) => {

// 	let newUser = new User({
// 		firstName : reqBody.firstName,
// 		lastName : reqBody.lastName,
// 		mobileNo : reqBody.mobileNo,
// 		email : reqBody.email,
// 		password : bcrypt.hashSync(reqBody.password, 10)
// 	})

// 	return User
// 	.find({ email : reqBody.email })
// 	.then(result => {
// 		if(result.length > 0) {
// 			return ('Email already exist! Try different email.');
// 		} else {
// 			return newUser
// 			.save()
// 			.then((user, error) => {
// 				if(error){
// 					return false
// 				} else {
// 				// 	return (`Email: ${user.email} \n\ \n\
// 				// (Successfully Created New User.)`);
// 					return user;
// 				}
// 			})
// 		}		
// 	})

// };




// User Registration
module.exports.registerUser = (reqBody) => {
	
	// Creates a variable named "newUser" and instantiates a new "User" object using the Mongoose model

	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		// password : reqBody.password
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {

		// User registration failed
		if(error){
			return false;

		// User registration successful 
		} else {
			return true;
		}
	})
}








module.exports.checkEmailExists = (reqBody) => {
	return User.find({ email : reqBody.email })
	.then(result => {
		if(result.length > 0){
			// return (`==>(${reqBody.email}) is Already Exist in Database.`);
			return false;
		} else  {
			// return ("User email does not exist in database. \n\ You can Register with this Email.")
			return ("User email does not exist in database.")

		}
	})
}


// Check if email exists
// module.exports.checkEmailExists = (reqBody) => {
// 	return User.find({email : reqBody.email}).then(result => {

// 		// The find method returns a record if a match is found
		
// 		if(result.length > 0){
// 			return true;
		
// 		// No duplicate emails found
// 		// The user is not registered in the database
// 		} else {
// 			return true;
// 		}
// 	})
// }





module.exports.loginUser = (reqBody) => {
	return User.findOne({ email : reqBody.email })
	.then(result => {
		if(result == null){
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				return { access : auth.createAccessToken(result) }
			} else {
				return false
			}
		}
	})
}




module.exports.getAllUsers = () => {
	return User.find({}).then((result, error) => {
		return result;
	})
}



module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "********";
		return result;

	});

};

 



module.exports.addToCart = async (data) => {


	const product = await Product.findById(data.productId)

	let isUserUpdated = await User.findById(data.userData.id).then(user =>{

			user.orderedProduct.push({
				products:[{
					productId : data.productId,
					productName : product.name,
					quantity : data.quantity
				}]
				// totalAmount: product.price * data.quantity
			})


				return user.save().then((user, error) =>  {
				if (error) {
					return false
				}else{
					return ('Ordered successfully!')
				}
			})
		})


	let isProductUpdated = await Product.findById(data.productId).then(product => {
		product.userOrders.push({userData : data.userData.id});

		return product.save().then((product, error) => {
			if(error){
				return false;
			} else {
				return true;
			};
		});
	});

	if(isUserUpdated && isProductUpdated){
		return isUserUpdated;
	} else {
		return false;
	}
}








// module.exports.enroll = async (req, res) => {
// 	const { productId, quantity } = req.body;
// 	const userData = auth.decode(req.headers.authorization)

// 	if (userData.isAdmin == true) {
// 		res.send("You're an Admin. Please use a non-Admin user to order a Product.");
// 	} else {
// 		// Order
// 		// Order schema for Postman, productName causes error
// 		/*
// 			{
// 				"productId" : product._id,
// 				"quantity" : number
// 			}
// 		*/
// 		let orderData = {

// 			userId: userData.id,
// 			productId,
// 			quantity
// 		};

// 		let product = await Product.findById(orderData.productId);

// 		if (product.isActive == true) {
// 			product.userOrders.push({ userId: orderData.userId });

// 			await product.save();

// 			//Checkout Details
// 			let user = await User.findById(orderData.userId);

// 			console.log(user);

// 			let newOrderedProduct = {

// 				products: [{

// 					productId: orderData.productId,
// 					productName: product.name,
// 					quantity: orderData.quantity
// 				}],
// 				totalAmount: product.price * orderData.quantity,
// 				purchasedOn: new Date()
// 			};

// 			user.orderedProduct.push(newOrderedProduct);

// 			await user.save();

// 			res.send("Ordered successfully");
// 		} else {
// 			res.send("Product out of stock. Please select other product");
// 		}		
// 	}
// };





module.exports.getUserDetails = (reqParams) => {
	return User.findById(reqParams.id)
	.then(result => {
		result.password = "*******"
		return (`Email: ${result.email} \n
				Orders: ${result.orderedProduct}`)
	})
}





module.exports.setAsAdminUser = (reqParams, reqBody) => {
	let setAdmin = {
		isAdmin : true
	}

	return User
	.findByIdAndUpdate(reqParams.id, setAdmin)
	.then((result, error) => {
		if(error){
			return false 
		} else {
			return (`Successfully Set As Admin User.`)
		}
	})
}



module.exports.checkOut = async (data) => {
	
	const product = await Product.findById(data.productId)
	let totalQty = data.totalAmount

	return User.findById(data.id, totalQty)
	.then((result, error) => {
		if(error){
			return false
		} else {
			return (`${result.name} ${result.totalQty}`)
		}
	})
}




module.exports.deleteUser = (userId) => {
	return User.findByIdAndRemove(userId)
	.then((result, error) => {
		if(error){
			return false;
		} else {
			return ('Successfully deleted user.');
		}
	}) 
}