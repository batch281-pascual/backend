const express = require('express');
const router = express.Router();
const auth = require('../auth');

const productController = require('../controllers/productController')

router.post("/addProduct", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	if(data.isAdmin == true){
		productController
		.addProduct(req.body)
		.then(resultFromController => res.send(resultFromController))
	} else {
		res.send('You are not ADMIN user. \n\ Contact your System Administrator!')
	}

})


router.get("/getAllProducts", (req, res) => {
	productController
	.getAllProducts(req.body)
	.then(resultFromController => res.send(resultFromController))
})

router.get("/", (req, res) => {
	productController
	.allActiveProducts()
	.then(resultFromController => res.send(resultFromController))
})


router.get("/inactiveProducts", auth.verify, (req, res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		productController.inactiveProducts(req.body)
		.then(resultFromController => res.status(200)
		.send(resultFromController))
	} else {
		res.send('Only admin user can view all the inactive products item.')
	}

})


router.get("/:id", (req, res) => {
	productController
	.singleProduct(req.params)
	.then(resultFromController => res.send(resultFromController))
})

router.patch("/updateProducts/:id", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	if(data.isAdmin	== true){
		productController
		.updateProduct(req.params, req.body)
		.then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Can't update product. \n\ You are not ADMIN user. \n\ Contact your System Administrator!")
	}
})


router.patch("/archive/:id", auth.verify, (req, res) => {

	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	} 

	if(data.isAdmin == true) {
		productController
		.archiveProduct(req.params, req.body)
		.then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Can't archive product. \n\ You are not ADMIN user. \n\ Contact your System Administrator!")
	}
})


router.patch("/activate/:id", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization);

	if(data.isAdmin == true){
		productController
		.activateProduct(req.params, req.body)
		.then(resultFromController => res.send(resultFromController))
	} else {
		res.send("Can't activate product. \n\ You are not ADMIN user. \n\ Contact your System Administrator!")
	}

})



router.delete("/deleteCourse/:id", auth.verify, (req, res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	if(data.isAdmin == true){
		productController.deleteCourse(req.params.id)
		.then(resultFromController => res.status(200)
		.send(resultFromController))
	} else {
		res.status(401)
		.send('Admin account only can delete product item')
	}
})




module.exports = router;