const express = require('express');
const router = express.Router();
const auth = require('../auth');

const userController = require('../controllers/userController');

// router.post("/register", (req, res) => {
// 	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
// })


// router.post("/register", auth.verify, (req, res) => {

// 	let data = {
// 		isAdmin : auth.decode(req.headers.authorization).isAdmin	
// 	} 

// 	if(data.isAdmin == true){
// 		userController.registerUser(req.body).then(resultFromController => res.status(200).send(resultFromController))		
// 	} else {
// 		res.status(401).send(`You're not ADMIN user. \n\ Contact your System Administrator!`)
// 	}
// })


router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
})




router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.status(200).send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.status(200).send(resultFromController))
})

router.get("/allUsers", (req, res) => {
	userController.getAllUsers(req.body).then(resultFromController => res.status(200).send(resultFromController))
})



router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);


	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController))
})





// router.post("/addToCart", auth.verify, (req, res) => {

// 	// let data = {
// 	// 	userData : auth.decode(req.headers.authorization),
// 	// 	productId : req.body.productId,
// 	// 	productName : req.body.productName,
// 	// 	quantity : req.body.quantity
// 	// }

// 	const { productId, quantity } = req.body;


// 	let data = {
// 			isAdmin : auth.decode(req.headers.authorization).isAdmin,
// 			userData : auth.decode(req.headers.authorization),
// 			productId,
// 			quantity
// 		};

// 		if(data.isAdmin === true){
// 			res.status(201)
// 			.send('Only Users Account can make orders.')		
// 		} else {
// 		return userController.addToCart(data)
// 					.then(resultFromController => res.status(200)
// 					.send(resultFromController))
// 		}
		
// })		



router.post("/addToCart", auth.verify, (req, res) => {

	let data = {
		userData : auth.decode(req.headers.authorization),
		productId : req.body.productId,
		quantity : req.body.quantity
	}

	userController.addToCart(data).then(resultFromController => res.send(resultFromController))
	// userController.enroll(data))

})


// router.post('/checkout', userController.checkout);










router.get("/getUserDetails/:id", (req, res) => {
	userController.getUserDetails(req.params)
	.then(resultFromController => res.status(200)
		.send(resultFromController))
})




router.patch("/setAsAdmin/:id", auth.verify, (req, res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin	
	} 

	if(data.isAdmin == true) {
		userController.setAsAdminUser(req.params, req.body)
		.then(resultFromController => res.status(200).send(resultFromController))	
	} else {
		res.status(401).send(`You're not eligible to SET AS ADMIN. \n\ Contact your System Administrator!`)
	}

	
})





router.delete("/deleteUser/:id", auth.verify, (req, res) => {
	const data = {
		isAdmin : auth.decode(req.headers.authorization).isAdmin	
	} 

	if(data.isAdmin == true) {
		userController.deleteUser(req.params.id)
		.then(resultFromController => res.status(200)
			.send(resultFromController))
	} else {
		res.status(401)
		.send('Admin only!')
	}
})




module.exports = router;