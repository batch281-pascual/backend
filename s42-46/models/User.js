const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName : {
		type : String
		// required : [true, "Firstname is required."]
	},
	lastName : {
		type : String
		// required : [true, "Lastname is required."]
	},
	mobileNo : {
		type : Number
		// required : [true, "Mobile No. is required."]
	},
	email : {
		type : String,
		required : [true, "Email Address is required."]
	},
	password : {
		type : String,
		required : [true, "Password is required."]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	orderedProduct : [{
		products : [{
			productId : {
				type : String, 
				required : [true, "Product ID is required."]
			},	
			productName : {
				type : String
				// required : [true, "Product Name is required."]
			},
			quantity : {
				type : Number,
				default: 0
			}
		}],
		totalAmount : {
			type : Number
		},
		purchasedOn : {
			type : Date,
			default : new Date()
		}
	}],

});

module.exports = mongoose.model('User', userSchema);