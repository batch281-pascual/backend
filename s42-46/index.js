const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRoutes = require('./routes/userRoutes')
const productRoutes = require('./routes/productRoutes')

const app = express();

// MongoDB ATLAS Connection String
mongoose.connect("mongodb+srv://mjordanpascual:FGRcaKmOh9QH41yO@wdc028-course-booking.sbuuomu.mongodb.net/E-Commerce-API?retryWrites=true&w=majority", {
	useNewUrlParser : true,
	useUnifiedTopology: true
});

// Mongoose Connection Verifier!
mongoose.connection.once('open', () => console.log('MongoDB ATLAS! Connection Established'))

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

	
app.use("/users", userRoutes )
app.use("/products", productRoutes)




app.listen(process.env.PORT || 4000, () => {
	console.log(`E-Commerce API - is running on port ${process.env.PORT || 4000}`)
});