// JSON Object
/*
	- JSON stands for JavaScript Object Notation
	Syntax:
		{
			"propertyA" : "valueA",
			"propteryB" : "valueB"
		}
*/
// JSON as objects
/*{
	"city": "Quezon city",
	"province": "Metro Manila",
	"country": "Philippines"
}*/

// JSON Arrays
/*"cities": [
	{
		"city": "Quezon city",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		"city": "Manila city",
		"province": "Metro Manila",
		"country": "Philippines"
	},
	{
		"city": "Makati city",
		"province": "Metro Manila",
		"country": "Philippines"
	}
]*/

// Mini Activity - Create a JSON Array that will hold three breeds of dogs with properties:

/*"dogs": [
	{
		"name": "Kookie",
		"age": "1",
		"breed": "Shitzu"
	},
	{
		"name": "Kichie",
		"age": "1",
		"breed": "American Bully"
	},
	{
		"name": "Nurf",
		"age": "1",
		"breed": "French Bulldog"
	}
]*/

// JSON Methods

// Convert Data into Stringified JSON

let batchesArr = [{ batchName: 'Batch X'}, { batchName: 'Batch Y'}];

// the "stringify" methods is used to convert JavaScript objects into a string
console.log('Result from stringify method: ');
console.log(JSON.stringify(batchesArr));

let data = JSON.stringify({
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
})

console.log(data);

// Using Stringify method with variables
// User details

// let firstName = prompt('What is your first name?');
// let lastName = prompt('What is your last name?');
// let age = prompt('What is your age?');
// let address = {
// 	city: prompt('Which city do you live in?'),
// 	country: prompt('Which country does your city address belong to?')
// };

// let otherData = JSON.stringify({
// 	firstName: firstName,
// 	lastName: lastName,
// 	age: age,
// 	address: address
// })

// console.log(otherData);

// Mini Activity - Create a JSON data that will accept user car details with variables brand, type, year.

// let	brand = prompt('Please enter car brand');
// let	type = prompt('Please enter car type');
// let	year = prompt('Please enter manufacture year');


// let carDetail = JSON.stringify({
// 	brand: brand,
// 	type: type,
// 	year: year
// })

// console.log(carDetail);

// Converting Stringified JSON into JavaScript Objects

let batchesJSON = `[{ "batchName": "Batch X" }, { "batchName": "Batch Y" }]`;

console.log('Result from prase method');
console.log(JSON.parse(batchesJSON));